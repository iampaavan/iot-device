'''
Created on Mar 17, 2019

@author: paavan gopala

'''

from coapthon.client.helperclient import HelperClient


class CoapClientConnector():
    '''
    Connection for CoAP Server
    '''
    
    def __init__(self, host, port, path):
        '''
        constructor
        @param host: IP/domain of the Sserver
        @param port: Port Number to Connect 
        @param path: Resource URI  
        '''
        self.host = host
        self.port = port
        self.path = path
        self.client = HelperClient(server=(host, port))
    
    def ping(self):
        '''
        Ping Function Definition for Server
        '''    
        self.client.send_empty("")
        
    def get(self):
        '''
        GET Request Function Definition
        '''
        response = self.client.get(self.path)
        print(response.pretty_print())
    
    def post(self, jsonData):
        '''
        POST Request Function Definition
        @param jsonData: message/data to POST
        '''
        response = self.client.post(self.path, jsonData)
        print(response.pretty_print())
    
    def put(self, jsonData):
        '''
        Wrapper method for the PUT action
        @param jsonData: message or data to PUT 
        '''
        response = self.client.put(self.path, jsonData)
        print(response.pretty_print())
        
    def delete(self):
        '''
        DELETE Request Function Definition
        ''' 
        response = self.client.delete(self.path)
        print(response.pretty_print())
        
    def stop(self):
        '''
        Function Definition to stop COAP Client
        '''  
        self.client.stop()     
