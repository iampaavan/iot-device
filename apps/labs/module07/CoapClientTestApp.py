'''
Created on Mar 17, 2019

@author: paavan gopala

'''
from labs.module07.CoapClientConnector import CoapClientConnector
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from labs.common.ConfigUtil import ConfigUtil
from labs.common.ConfigConst import ConfigConst

# ConfigUtil class --> Instance Variable
config = ConfigUtil()

# Config File Loading
config.loadConfig()

# DataUtil class --> Instance Variable
data = DataUtil()

# ip/domain of the server 
host = config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.HOST_KEY)

# port number to connect
port = int(config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.PORT_KEY))

# URI Resource
path = 'temperature'

# SensorData class --> Instance Variable
sensorData = SensorData()

# add new SensorData value
sensorData.addValue(10.00)

#  CoapClient connector called
coapClient = CoapClientConnector(host, port, path)

# ping request
coapClient.ping()

# CoapClient get request
coapClient.get()
  
# CoapClient post request
coapClient.post(data.SensordataToJson(sensorData))  

# add new value to sensor data
sensorData.addValue(5.00)

# CoapClient put request
coapClient.put(data.SensordataToJson(sensorData))  

# CoapClient delete request
coapClient.delete()  

# Stop Method called to stop CoapClient
coapClient.stop()

