'''
Created on Mar 17, 2019

@author: paavan gopala

'''
from labs.module07.CoapServerConnector import CoapServerConnector

# COAP Server Connector Initialization
server = CoapServerConnector()

# Kick-Off the Server
server.start()
