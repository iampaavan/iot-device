'''
Created on January 19, 2019

@author: Paavan Gopala
'''
from labs.module02 import TempSensorEmulator

'''initialize TempSensorEmulator class and pass the 
parameter '5' to the constructor to set the alert difference'''
My_Thread_t1 = TempSensorEmulator.TempSensorEmulator(5)

'''passing the boolean parameter setEmulator method (to True) '''
My_Thread_t1.setEmulator(True)  

'''Start the user defined thread --> (My_Thread_t1) '''
My_Thread_t1.start() 
