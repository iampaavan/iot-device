'''
Created on Jan 19, 2019

@author: Paavan Gopala
'''

from labs.common import ConfigUtil

'''SMTP/ESMTP client class''' 
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class SmtpClientConnector(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
Constructor
        '''
        
        ''' initialize the ConfigUtil class by calling it's constructor''' 
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig() # load all the desired configurations
        
        print('Configuration data: ' + str(self.config.getConfigData(False)))
              
    def publishMessage(self, topic, data):
        host = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.HOST_KEY)
        port = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.PORT_KEY)
        fromAddr = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.FROM_ADDRESS_KEY)
        toAddr = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.TO_ADDRESS_KEY)
        authToken = self.config.getProperty(self.config.configConst.SMTP_CLOUD_SECTION, self.config.configConst.USER_AUTH_TOKEN_KEY)
        
        #setting up the message details -->from, to, subject to send out the Email Alert
        msg = MIMEMultipart()
        msg['from'] = fromAddr
        msg['to'] = toAddr
        msg['subject'] = topic
        msgBody = str(data)
        msg.attach(MIMEText(msgBody))
        msgText = msg.as_string()
        
        # send e-mail notification
        smtpServer = smtplib.SMTP_SSL(host, port)
        smtpServer.ehlo()
        smtpServer.login(fromAddr, authToken)
        smtpServer.sendmail(fromAddr, toAddr, msgText)
        smtpServer.close()
        
