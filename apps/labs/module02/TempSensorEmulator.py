'''
Created on Jan 19, 2019

@author: Paavan Gopala
'''
'''Thread module emulating a subset of Java's threading model.'''
import threading 
from labs.common import SensorData
from labs.module02 import SmtpClientConnector
from time import sleep
import random

'''Thread implementation by using threading library'''
''' Creating a class "TempSensorEmulator" to emulate the temperature sensor related data'''


class TempSensorEmulator(threading.Thread):
   
    '''This constructor (__init__) will always be called with keyword arguments'''   

    def __init__(self, alertdiff):
        threading.Thread.__init__(self)
        self.enableEmulator = False
        self.sensordata = SensorData.SensorData()  # initialize the SensorData Module
        # initialize the SmtpClientConnector Module
        self.connector = SmtpClientConnector.SmtpClientConnector()  
        self.timeInterval = 5  # setting the timeInterval to 5 seconds for sensing the Temperature.
        self.alertDiff = alertdiff  # initializing the variable 'alertdiff'
        self.lowValue = 0  # setting the lowValue variable to 0 degrees
        self.highValue = 30  # setting the highValue variable to 30 degrees
        # Using the random library function to generate random temperatures btwn 0 and 30 degrees
        self.curTemp = random.uniform(float(0), float(30))
        TempSensorEmulator.__instance = self
            
    def setEmulator(self, boolean):
        self.enableEmulator = boolean
    
    '''run() method invokes the callable object 
    passed to the object's constructor as the
    target argument'''
          
    def run(self):
        threading.Thread.run(self) 
        '''Method run () representing the thread's activity'''
        while True:
            ''' the while loop runs indefinitely'''     
            if(self.enableEmulator):
                
                '''the above if condition for the
                boolean condition of enableEmulator variable'''
                self.curTemp = random.uniform(float(self.lowValue), float(self.highValue))
                ''' set curTem with lowValue & highValue temperature values'''
                self.sensordata.addValue(self.curTemp)
                print('\n * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *')
                print('Emulated Sensor Readings:')
                print(str(self.sensordata))  # print all the sensor related data
                
                ''' the below if condition checks if the curTemp exceeds
                the AvgValue by alertdiff, here in our case is 5 degrees'''
                
                if(abs(self.curTemp - self.sensordata.getAvgValue()) >= self.alertDiff):
                    print()
                    
                    ''' the 2 below print statements, prints the below mentioned messages in the console'''
                    
                    print('Current Temperature exceeds Average Temperature by:' + ' ' + str(self.alertDiff) + ' degrees') 
                    print('Triggering an Email Alert Message --> sent to --> iampaavan.iot@gmail.com')
                    
                    ''' The below publicMessage - Sends out an Email Alert Message to 
                    configured by the User.''' 
                    self.connector.publishMessage('Temperature Alert Message', self.sensordata)
                
                sleep(self.timeInterval)  # Delay execution for a given number of seconds
        
