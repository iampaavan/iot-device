'''
Created on Mar 6, 2019

@author: Paavan Gopala
'''
from labs.module06 import MqttClientConnector
from time import sleep

# MqttClientConnector instance
connector = MqttClientConnector.MqttClientConnector()

# Connecting to broker
connector.connect(None, None)

# subscribing to a topic
connector.subscibetoTopic("Customized Subscription To Topic")

# wait for 30 seconds
sleep(30)

# unsubscribe from topic
connector.unsubscibefromTopic("Un-Subscribe from Topic")

# disconnecting from broker
connector.disconnect()
