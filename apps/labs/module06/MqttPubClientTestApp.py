'''
Created on Mar 2, 2019

@author: Paavan Gopala
'''
from labs.module06 import MqttClientConnector

# instance variable for MqttClientConnector
connector = MqttClientConnector.MqttClientConnector()

# Connecting to broker
connector.connect(None, None)

# Publishing message to topic mytest
connector.publishMessage("Publish My Message", connector.message(), 2)

# Disconnect
connector.disconnect()
