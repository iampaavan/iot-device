'''
Created on Mar 5, 2019

@author: Paavan Gopala
'''

import time
import paho.mqtt.client as mqttClient
from labs.common import ConfigUtil
from labs.common import SensorData
from time import sleep
from datetime import datetime
from labs.common import DataUtil


class MqttClientConnector(object):
    '''
    MQTT connector
    Functions to connect to broker, subscribe message , publish message
    disconnect from broker 
    @param port: the port to which broker connects
    @param brokerAddr: the domain name of broker
    @param mqttClient: instance variable for MqttClient
    @param config: instance variable for ConfigUtil class   
    '''
    port = None
    brokerAddr = ""
    brockerKeepAlive = None
    mqttClient = None
    config = None

    def __init__(self):
        '''
        Constructor
        '''
        self.mqttClient = mqttClient.Client()
        self.config = ConfigUtil.ConfigUtil()
        self.sensoData = SensorData.SensorData()
        self.datautil = DataUtil.DataUtil()
        self.config.loadConfig()
        self.brokerAddr = self.config.getProperty(self.config.configConst.MQTT_CLOUD_SECTION, self.config.configConst.CLOUD_MQTT_BROKER)
        self.port = int(self.config.getProperty(self.config.configConst.MQTT_CLOUD_SECTION, self.config.configConst.CLOUD_MQTT_PORT))
        self.brockerKeepAlive = int(self.config.getProperty(self.config.configConst.MQTT_CLOUD_SECTION, self.config.configConst.KEEP_ALIVE_KEY))
        self.connected_flag = False
                
    def connect(self, connectionCallback=None , msgCallback=None):
        '''
        Function to Establish Connection --> MQTT broker
        '''
        # Setting CallBacks
        if(connectionCallback != None):
            self.mqttClient.on_connect = connectionCallback
        else:
            self.mqttClient.on_connect = self.onConnect
            
        if(msgCallback != None) :
            self.mqclient.on_disconnect = msgCallback
        else :
            self.mqttClient.on_disconnect = self.onMessage
        # callback when message arrives
        self.mqttClient.on_message = self.onMessage    
        self.mqttClient.loop_start()
        print("Connecting to broker", self.brokerAddr)
        # connect to broker
        self.mqttClient.connect(self.brokerAddr, self.port, self.brockerKeepAlive)
        while not self.connected_flag:
            print("In wait loop")
            time.sleep(1)
        
    def disconnect(self):
        '''
        function to disconnect from broker
        '''
        print("Disconneting the MQTT  broker connection ")
        self.mqttClient.disconnect()
        
    def onConnect(self , client , userData , flags , returncode):
        '''
        callback when the connection is made with broker
        @param rc: return code for connection  
        '''
        if returncode == 0:
            self.connected_flag = True
            print("Connected OK returned Code:" , returncode)
        else:
            print("Bad connection Returned Code:", returncode)
            
    def onMessage(self , client , userdata , msg):
        '''
        callback when message arrives
        '''
        print("Topic is " + msg.topic + "-->" + str(msg.payload))
        
    def publishMessage(self , topic , msg , qos=2):
        '''
        function to publish message
        @param topic: name of the topic to publish message
        @param msg: The message to be sent   
        '''
        print("Publishing:", msg)
        self.mqttClient.publish(topic, msg, qos)
        sleep(35)
    
    def subscibetoTopic(self , topic , connnectionCallback=None, qos=2):
        '''
        function to subscribe to a topic
        @param topic: name of the topic to subscribe to 
        @param connectionCallback:   
        '''
        if connnectionCallback != None:
            self.mqttClient.on_subscribe = (connnectionCallback)
            self.mqttClient.on_message = (connnectionCallback)
        self.mqttClient.subscribe(topic , qos)
        
    def unsubscibefromTopic(self , topic):
        '''
        function to unsubscribe to a topic
        '''       
        print("Unsubscribing from topic", topic)
        self.mqttClient.unsubscribe(topic)
        
    def message(self):
        '''
        sensordata message to publish
        '''
        self.sensoData.curValue = 30
        self.sensoData.avgValue = 25
        self.sensoData.maxValue = 40
        self.sensoData.minValue = 6
        self.sensoData.timeStamp = str(datetime.now())
        self.sensoData.samples = 6
        self.senmessage = self.datautil.SensordataToJson(self.sensoData)
        return self.senmessage
