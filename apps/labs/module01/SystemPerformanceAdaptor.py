'''
Created on January 16, 2019

@author: Paavan Gopala
'''
from time import sleep
import threading  # Thread module emulating a subset of Java's threading model
import psutil  # psutil is a cross-platform library for retrieving information on running processes and system utilization (CPU, memory, disks, network, sensors) in Python


class SystemPerformanceAdaptor(threading.Thread):  # (Thread) A class that represents a thread of control.
    
    def system_statistics(self):  # Function or method to print the CPU and Memory related informations
        
        print('\n * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *')
        print('My HP Pavilion Notebook - System Performance Readings:')
        print('CPU Statistics : ' + str(psutil.cpu_stats()))  # Returns CPU statistics
        print('Memory : ' + str(psutil.virtual_memory()))  # Returns statistics about system memory usage
        print('My Thread Name: ' + threading.current_thread().getName())  # Prints the thread name currently executing
        sleep(10)  # Delay execution for a period of 10 seconds, which means it prints the system related information for every 10 seconds
        
    def run(self):
           
            self.system_statistics()  # Run or trigger the system_statistics method or function

