'''
Created on January 16, 2019

@author: Paavan Gopala 
'''
from labs.module01 import SystemPerformanceAdaptor # import SystemPerformanceAdaptor as a class
from time import sleep


def __main__():

            My_Thread_t1 = SystemPerformanceAdaptor.SystemPerformanceAdaptor() #initialize SystemPerformanceAdaptor
            My_Thread_t1.daemon = True
            
            My_Thread_t1.start() # Start the user defined thread --> (My_Thread_t1)
            sleep(10) # Delay execution for a period of 10 seconds


while(__name__ == '__main__'): #check the condition in the while loop for __main__ and run infinitely
    
    __main__() # Run the __main__ method and run indefinitely
