'''
Created on January 26, 2019

@author: Paavan Gopala
'''
import sys
import os
# sys.path.insert(0,'/home/pi/workspace/iot-device/connected-devices-python/apps')
# pwd = os.getcwd() + '/apps/'
pwd = os.getcwd() + '/apps'
sys.path.insert(0, pwd)
print(sys.path)

from labs.module05 import TempSensorAdaptor

'''initialize TempSensorAdaptor class and pass the 
parameter '5' to the constructor to set the alert difference'''
My_Thread_t1 = TempSensorAdaptor.TempSensorAdaptor(5)

'''passing the boolean parameter setEmulator method (to True) '''
My_Thread_t1.setEmulator(True)  

'''Start the user defined thread --> (My_Thread_t1) '''
My_Thread_t1.start() 
