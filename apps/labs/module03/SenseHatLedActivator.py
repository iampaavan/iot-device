'''
Created on Jan 26, 2019

@author: Paavan Gopala
'''

from time import sleep
from sense_hat import SenseHat
import threading

'''
declare 2 tuples to change the background and
text color on SenseHat
'''
blue = (0, 0, 255) 
yellow = (255, 255, 0)


class SenseHatLedActivator(threading.Thread):
    
    enableLed = False
    rateInSec = 1
    rotateDeg = 270
    sh = None
    displayMsg = None

    def __init__(self, rotateDeg=270, rateInSec=1):
        super(SenseHatLedActivator, self).__init__()

        if rateInSec > 0:
            self.rateInSec = rateInSec
        
        if rotateDeg >= 0:
            self.rotateDeg = rotateDeg
            self.sh = SenseHat()
            self.sh.set_rotation(self.rotateDeg)

    def run(self):
        while True:
            
            if self.enableLed:
                
                if self.displayMsg != None:
                    #display message 'increase' or 'decrease' Temp based on current Temp readings
                    self.sh.show_message(str(self.displayMsg), text_colour=yellow, back_colour=blue, scroll_speed=0.05)
                
                else:
                    self.sh.show_letter(str('R')) #if no message, display 'R' on SenseHat
                sleep(self.rateInSec)
                self.sh.clear()
            
            sleep(self.rateInSec)

    def getRateInSeconds(self):
        return self.rateInSec
    
    '''
    method to enable the flag on SenseHat
    '''
   
    def setEnableLedFlag(self, enable):
        self.sh.clear()
        self.enableLed = enable
        
    '''
    method to set the message that
    needs to be displayed on SenseHat
    '''   

    def setDisplayMessage(self, msg):
        self.displayMsg = msg
