'''
Created on January 26, 2019

@author: Paavan Gopala
'''

from labs.common import ActuatorData
from labs.module03 import SenseHatLedActivator


class TempActuatorEmulator(object):
    '''
    classdocs
    '''
    actuatorData = None
    senseHat = None
    
    def __init__(self):
        '''
        Constructor
        '''
        # instance of ActuatorData()
        self.actuatorData = ActuatorData.ActuatorData()
        # instance of SenseHatLedActivator()
        self.senseHat = SenseHatLedActivator.SenseHatLedActivator()
        
    '''
    below method to public message on console or
    on SenseHat whenever the temperature increases or decreases
    the nominal temperature
    '''   
        
    def processMessage(self, actuatorData):
        
        if(self.actuatorData != actuatorData):
            self.senseHat.setEnableLedFlag(True)
            
            # check if current temperature increases more than nominal temperature
            if(actuatorData.getValue() > 0):
                self.senseHat.setDisplayMessage('Reduce Temp by: ' + str(actuatorData.getValue()))
                print('       \n Reduce Temp by:' + str(actuatorData.getValue()))
                
                self.actuatorData.updateData(actuatorData)
             
            # check if current temperature decrease below nominal temperature   
            elif(actuatorData.getValue() < 0):
                self.senseHat.setDisplayMessage('Up the Temp by: ' + str(actuatorData.getValue()))
                print('       \n Up the Temp by: ' + str(abs(actuatorData.getValue())))
                
                # update the actuator data every time the temperature readings are fetched
                self.actuatorData.updateData(actuatorData)
