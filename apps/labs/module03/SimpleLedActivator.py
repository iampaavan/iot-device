'''
Created on January 26, 2019

@author: Paavan Gopala
'''

from time import sleep
import threading
import RPi.GPIO as GPIO


class SimpleLedActivator(threading.Thread):
    '''
    classdocs
    '''
    enableLed = False
    rateInSec = 1
    
    def __init__(self, rateInSec=1):
        '''
        Constructor
        '''
        super(SimpleLedActivator, self).__init__()
        if(rateInSec > 0):
            self.rateInSec = rateInSec
            
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(17, GPIO.LOW)
        
    def run(self):
        while True:
            if self.enableLed:
                GPIO.output(17, GPIO.HIGH)
                sleep(self.rateInSec)
                GPIO.output(17, GPIO.LOW)
        sleep(self.rateInSec)
        
    def getRateInSeconds(self):
        return self.rateInSec
    
    def setEnableLedFlag(self, enable):
        GPIO.setup(17, GPIO.LOW)
        self.enableLed = enable
