'''
Created on January 26, 2019

@author: Paavan Gopala
'''
import threading
from labs.common import SensorData
from labs.module02 import SmtpClientConnector
from labs.common import ConfigUtil
from labs.common import ActuatorData
from sense_hat import SenseHat
from labs.module03 import TempActuatorEmulator
from time import sleep
import random


class TempSensorAdaptor(threading.Thread):
    
    # create an instance variable for configutil
    config = ConfigUtil.ConfigUtil()
    # sensehat parameter or variable --> initialized to None 
    sh = None
    
    def __init__(self, alertdiff):
        '''
        constructor
        @param enableEmulator: boolean to enable the emulator
        @param sensorData: instance of sensorData class
        @param connector: instance of smtpClientConnector class
        @param timeInterval: the duration after which new temp value is generated
        @param alertDiff: the threshold value for sending alert message
        @param lowValue: lowest value for the temperature
        @param highValue: highest value for the temperature
        @param curTemp: current value for the temperature 
        @param nominalTemp: setting it to 20 degrees      
        '''
        threading.Thread.__init__(self)
        self.enableEmulator = False
        self.actuator = ActuatorData.ActuatorData()
        self.actuatorEmulator = TempActuatorEmulator.TempActuatorEmulator()
        self.sensorData = SensorData.SensorData()
        self.sh = SenseHat()
        self.connector = SmtpClientConnector.SmtpClientConnector()
        self.timeInterval = int(self.config.getProperty(self.config.configConst.CONSTRAINED_DEVICE, self.config.configConst.POLL_CYCLES_KEY))
        self.alertDiff = alertdiff
        self.nominalTemp = 20
        self.lowValue = 0
        self.highValue = 30
        self.curTemp = 0
            
    def setEmulator(self, boolean):
        '''
        set the boolean value to start the emulator
        '''
        self.enableEmulator = boolean
        
    '''run() method invokes the callable object 
    passed to the object's constructor as the
    target argument'''    
        
    def run(self):
        '''
        this function is called when thread starts
        '''
        threading.Thread.run(self)
        '''Method run () representing the thread's activity'''
        
        while True:
            
            if(self.enableEmulator):
                # generate random temperature value between 0 and 30
                #self.curTemp = random.uniform(float(self.lowValue), float(self.highValue))
                # get the temperature readings directly from SenseHat device
                self.curTemp = self.sh.get_temperature()
                # add the value to sensor data
                self.sensorData.addValue(self.curTemp)
                print('\n * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *')
                print('SenseHat Temperature Readings:')
                # print the sensor data 
                print(str(self.sensorData))
                
                ''' the below if condition checks if the curTemp exceeds
                the AvgValue by alertdiff, here in our case is 5 degrees'''
                
                if(abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                    
                    ''' the 2 below print statements, prints the below mentioned messages in the console'''
                    print('Current Temperature exceeds Average Temperature by: ' + str(self.alertDiff) + ' degrees')
                    print('Triggering an Email Alert Message --> sent to --> iampaavan.iot@gmail.com')
                    self.connector.publishMessage('Warning !!! Email Temperature Alert Message', self.sensorData)
                 
                # check condition if current temperature exceeds set nominal temperature  
                if(self.curTemp > self.nominalTemp):
                   
                    self.actuator.setCommand(ActuatorData.COMMAND_ON)
                    self.actuator.setStatusCode(ActuatorData.STATUS_ACTIVE)
                    self.actuator.setErrorCode(ActuatorData.ERROR_OK)
                    self.actuator.setStateData('Decrease')
                    self.actuator.setValue(self.curTemp - self.nominalTemp)
                    self.actuatorEmulator.processMessage(self.actuator)
                
                # check condition if current temperature falls below the set nominal temperature      
                elif(self.curTemp < self.nominalTemp):
                
                    self.actuator.setCommand(ActuatorData.COMMAND_OFF)
                    self.actuator.setStatusCode(ActuatorData.STATUS_ACTIVE)
                    self.actuator.setErrorCode(ActuatorData.ERROR_OK)
                    self.actuator.setStatusCode('Increase')
                    self.actuator.setValue(self.curTemp - self.nominalTemp)
                    self.actuatorEmulator.processMessage(self.actuator)
                    
                # wait for 5s --> fetching data from ConnectDevicesConfig.props file
                sleep(self.timeInterval)
        
