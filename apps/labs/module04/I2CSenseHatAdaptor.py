'''
Created on Feb 7, 2019

@author: Paavan Gopala
'''

import smbus
import threading

from time import sleep
from labs.common import ConfigUtil

# initialize the variables
i2cBus = smbus.SMBus(1)  # Use I2C bus No.1 on Raspberry Pi3 +
enableControl = 0x2D
enableMeasure = 0x08
accelAddr = 0x1C  # address for IMU (accelerometer)
magAddr = 0x6A  # address for IMU (magnetometer)
pressAddr = 0x5C  # address for pressure sensor
humidAddr = 0x5F  # address for humidity sensor
begAddr = 0x28  # starting address of the sensor module
totBytes = 6  # read a total of 6 bytes from the respective sensor
DEFAULT_RATE_IN_SEC = 5  # wait for a period of 5 seconds

'''
This is a I2C - SenseHat class
'''
class I2CSenseHatAdaptor(threading.Thread):
    
    rateInSec = DEFAULT_RATE_IN_SEC
    
    def __init__(self):
        super(I2CSenseHatAdaptor, self).__init__()
        self.config = ConfigUtil.ConfigUtil()
        self.config.loadConfig()
        print('Configuration data...\n' + str(self.config))
        self.initI2CBus()
    
    '''
    Method to initialize the I2C bus &
    enable each I2C sensor addresses respectively
    '''

    def initI2CBus(self):
        print("Initializing I2C bus and enabling I2C addresses...")
        
        i2cBus.write_byte_data(accelAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(magAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(pressAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(humidAddr, enableControl, enableMeasure)
        
    '''
    enabling I2C bus at respective 'address' of sensor
    to allow read updates
    '''

    def enableMeasurement(self, address):
        i2cBus.write_byte_data(address, enableControl, enableMeasure)

    '''
    Function to begin the temperature emulator.
    @param value: True to start the emulator else False.  
    '''

    def setEmulator(self, value):
        self.enableEmulator = value 
    
    '''
    Read the accelerator data from the accelerometer address
    and the display the value on the console.
    @param accelAddr: 0x1C
    @param  begAddr: 0x28
    @param totByte: 6
    '''
    
    def displayAccelerometerData(self):
        self.enableMeasurement(accelAddr)
        sleep(DEFAULT_RATE_IN_SEC)
        data = i2cBus.read_i2c_block_data(accelAddr, begAddr, totBytes)
        print("\nAccelerometerData : " + str(data))
    
    '''
    Read the Magnetometer data from the magnetometer address
    and the display the value on the console.
    @param magAddr: 0x6A
    @param  begAddr: 0x28
    @param totByte: 6
    '''
    
    def displayMagnetometerData(self):
        self.enableMeasurement(magAddr)
        sleep(DEFAULT_RATE_IN_SEC)
        data = i2cBus.read_i2c_block_data(magAddr, begAddr, totBytes)
        print("\nMagnetometerData : " + str(data))
    
    '''
    Read the Pressure data from the pressure address
    and the display the value on the console.
    @param pressAddr: 0x5C
    @param  begAddr: 0x28
    @param totByte: 6
    '''
    
    def displayPressureData(self):
        self.enableMeasurement(pressAddr)
        sleep(DEFAULT_RATE_IN_SEC)
        data = i2cBus.read_i2c_block_data(pressAddr, begAddr, totBytes)
        print("\nPressureData : " + str(data))
           
    '''
    Read the Humidity data from the humidity address
    and the display the value on the console.
    @param humidAddr: 0x5F
    @param  begAddr: 0x28
    @param totByte: 6
    '''
    
    def displayHumidityData(self):
        self.enableMeasurement(humidAddr)
        sleep(DEFAULT_RATE_IN_SEC)
        data = i2cBus.read_i2c_block_data(humidAddr, begAddr, totBytes)
        print("\nHumidityData : " + str(data))

    '''
    run method loops indefinitely and calls all the 
    4 methods: Accelerometer, Magnetometer,Pressure and
    Humidity.
    '''
    
    def run(self):
        while True:
            if self.enableEmulator:
                self.displayAccelerometerData()  # call the accelerator sensor method
                self.displayMagnetometerData()  # call the Magnetometer sensor method
                self.displayPressureData()  # call the pressure sensor method
                self.displayHumidityData()  # call the Humidity sensor method
            
            sleep(self.rateInSec)  # sleep for a period of 5 seconds
  
