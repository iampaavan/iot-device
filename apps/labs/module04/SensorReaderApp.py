'''
Created on January 26, 2019

@author: Paavan Gopala
'''
import sys
import os


PWD = os.getcwd() + '/apps' # get the current working directory and append apps to the directory to run your code
sys.path.insert(0,PWD)  #Insert present working directory
print(sys.path) #print the path where the code is currently running.


from labs.module04 import I2CSenseHatAdaptor


MyThread = None # Instance variable for I2CSenseHatAdaptor 


MyThread = I2CSenseHatAdaptor.I2CSenseHatAdaptor() # Fetching the instance of I2CSenseHatAdaptor


MyThread.daemon = True  #Daemon initializer


MyThread.setEmulator(True)  # enabling the thread to begin I2CSenseHatAdaptor


MyThread.start()    #Run the I2CSenseHatAdaptor thread


#Run this while loop indefinitely
while(True):
    pass
