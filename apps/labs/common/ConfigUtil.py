'''
Created on January 19, 2019

@author: Paavan Gopala

'''

import configparser
import os
from labs.common import ConfigConst


class ConfigUtil(object):
    '''
    this class reads, loads and get the 
    properties from configuration file
    '''
    configData = None
    isLoaded = False
    configConst = None
    
    def __init__(self):
        '''
        Constructor
        '''
        self.configConst = ConfigConst.ConfigConst()
        self.configData = configparser.ConfigParser()
        self.configFilePath = self.configConst.DEFAULT_CONFIG_FILE_NAME
        
    def loadConfig(self):
        '''
        Loads the configuration data by checking the configuration file
        from the desired configuration path
        '''
        if(os.path.exists(self.configFilePath)):
            self.configData.read(self.configFilePath)
            self.isLoaded = True
           
    def getConfigData(self, forceReload=False):
        '''
        Get all the configuration data
        '''
        if(self.isLoaded == False or forceReload):
            # check the condition if the configuration has loaded or not'''
            self.loadConfig()
        return self.configData
    
    def getConfigFile(self):
        '''
        Fetch the configuration file and this
        method returns the path where the configuration
        file is stored
        '''
        return self.configFilePath
    
    def getProperty(self, section, key):
        ''' fetch all the relevant key values from section of ConfigConst'''
        return self.getConfigData(forceReload=False).get(section, key)

    def isConfiguredDataLoaded(self):
        ''' this method checks whether the configured data is loaded
        or not and returns a boolean value.'''
        return self.isLoaded   
        
