'''
Created on January 19, 2019

@author: Paavan Gopala
'''

import os
from datetime import datetime


class SensorData(object):
    '''
    @param timeStamp: gives the current date & time 
    @param curValue: gives the current temperature value
    @param avgValue: gives the average temperate data
    @param minValue: gives the min. temperature data
    @param maxValue: gives the max. temperature data
    @param totValue: gives the total temperature value
    @param samples: gives all the samples count as soon as the alert message is sent out    
    '''
    timeStamp = None
    curValue = 0
    avgValue = 0
    minValue = 30
    maxValue = 0
    totValue = 0
    samples = 0
    
    def __init__(self):
        '''
        method to get the current data & time
        '''
        self.timeStamp = str(datetime.now())
        
    def addValue(self, newVal):
        self.samples += 1  # increment the sample variable whenever new temperature value is detected
        self.timeStamp = str(datetime.now())
        self.curValue = newVal  # set the newly generated\sensed temperature value to curValue
        self.totValue += newVal
        if (self.curValue < self.minValue):
            # checks the condition and sets the min. temp value
            self.minValue = self.curValue
        if (self.curValue > self.maxValue):
            # checks the condition and sets the max. temp value
            self.maxValue = self.curValue
        if (self.totValue != 0 and self.samples > 0):
            # checks the condition and sets the avg. temp value
            self.avgValue = self.totValue / self.samples
            
    def getAvgValue(self):
        '''
        method gets the average value and returns it
        '''
        return self.avgValue
    
    def getMaxValue(self):
        '''
        method gets the max. temp value and returns it
        '''
        return self.maxValue
    
    def getMinValue(self):
        '''
        method gets the min. temp value and returns it
        '''
        return self.minValue
    
    def getValue(self):
        return self.curValue
    
    def __str__(self):
        '''
        ___str__ method basically consists of string values 
        required to time on the console by reading the desired
        temperature parameters from TempSensorEmulator
        '''
        customStr = \
        str(
        os.linesep + '\tTime: ' + self.timeStamp + \
        os.linesep + '\tCurrent: ' + str(self.curValue) + \
        os.linesep + '\tAverage: ' + str(self.avgValue) + \
        os.linesep + '\tSamples: ' + str(self.samples) + \
        os.linesep + '\tMin: ' + str(self.minValue) + \
        os.linesep + '\tMax: ' + str(self.maxValue))
        return customStr  # return the temperature Sensor related data
        
