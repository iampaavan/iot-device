'''
Created on Feb 15, 2019

@author: Paavan Gopala
'''
from labs.common import SensorData
from labs.common import ActuatorData
import json


class DataUtil(object):
    '''
    classdocs
    '''
    sensorData = None
    actuatorData = None
    jsonSd = None
    jsonAd = None

    def __init__(self):
        '''
        
        Constructor
        '''
        self.sensorData = SensorData.SensorData()
        self.actuatorData = ActuatorData.ActuatorData()
        
    def SensordataToJson(self, sensordata):
        self.jsonSd = json.dumps(sensordata.__dict__)
        outputSd = open('sensordata.txt', 'w')
        outputSd.write(self.jsonSd)
        return self.jsonSd
    
    def JsonToSensorData(self, jsondata):
        sdDict = json.loads(jsondata)
        
        self.sensorData.timeStamp = sdDict['timeStamp']
        self.sensorData.curValue = sdDict['curValue']
        self.sensorData.avgValue = sdDict['avgValue']
        self.sensorData.minValue = sdDict['minValue']
        self.sensorData.maxValue = sdDict['maxValue']
        self.sensorData.totValue = sdDict['totValue']
        self.sensorData.samples = sdDict['samples']
        return self.sensorData
    
    def ActuatorDataToJson(self, actuatordata):
        self.jsonAd = json.dumps(actuatordata.__dict__)
        outputAd = open('actuatordata.txt', 'w')
        outputAd.write(self.jsonAd)
        return self.jsonAd
    
    def JsonToActuatorData(self, jsondata):
        adDict = json.loads(jsondata)
        
        self.actuatorData.timeStamp = adDict['timeStamp']
        self.actuatorData.name = adDict['name']
        self.actuatorData.hasError = adDict['hasError']
        self.actuatorData.command = adDict['command']
        self.actuatorData.errCode = adDict['errCode']
        self.actuatorData.statusCode = adDict['statusCode']
        self.actuatorData.stateData = adDict['stateData']
        self.actuatorData.val = adDict['val']
        return self.actuatorData
